# Slime Agents

Real time simulation inspired by Sebastian Lague's [Coding Adventure: Ant and Slime Simulations](https://www.youtube.com/watch?v=X-iSQQgOd1A)

Should run on a Raspberry Pi 4 at ~40 fps (with `SDLW_BUFFER_RESOLUTION_DIVIDER 2` in `constants.h`)

Make sure to plug a DualShock (tested with PS4) controller

## Build

- Make sure you have CMake and G++ installed

```
sudo apt install build-essential
```

- Install SDL 2 and SDL 2 TTF

```
sudo apt install libsdl2-dev
sudo apt install libsdl2-ttf-dev
```

- Clone this repo anywhere you like
- Run `runpi.sh`

_`SlimeAgents` is copied to your home directory after build, and run from there_
