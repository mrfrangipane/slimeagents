#ifndef SDLTEST_FASTRANDOM_H
#define SDLTEST_FASTRANDOM_H

struct FastRandom {
    unsigned long x_=123456789, y_=362436069, z_=521288629;
    unsigned long xorshf96() {
        unsigned long t;
        x_ ^= x_ << 16;
        x_ ^= x_ >> 5;
        x_ ^= x_ << 1;

        t = x_;
        x_ = y_;
        y_ = z_;
        z_ = t ^ x_ ^ y_;

        return z_;
    }
};

#endif //SDLTEST_FASTRANDOM_H
