#ifndef SDLTEST_TASKQUEUE_H
#define SDLTEST_TASKQUEUE_H

#include <queue>
#include <mutex>
#include <atomic>

struct BlurTask {
    uint startRow;
    uint endRow;
};

struct CopyToTargetTask {
    uint startRow;
    uint endRow;
};

struct UpdateAgentsTask {
    uint startAgent;
    uint endAgent;
};

class TaskQueue {
private:
    std::queue<BlurTask> queueBlur;
    std::queue<CopyToTargetTask> queueCopyToTarget;
    std::queue<UpdateAgentsTask> queueUpdateAgents;
    std::mutex mutex;
    std::atomic<uint> runningTaskCount = 0;
    std::atomic<bool> runningVar = true;

public:
    void push(BlurTask blurTask);
    bool next(BlurTask& blurTask);

    void push(CopyToTargetTask copyToTargetTask);
    bool next(CopyToTargetTask& copyToTargetTask);

    void push(UpdateAgentsTask updateAgentsTask);
    bool next(UpdateAgentsTask& updateAgentsTask);

    void taskDone() { runningTaskCount--; }
    bool busy() { return runningTaskCount > 0; }
    bool running() { return runningVar; }
    void stop() { runningVar = false; }
};


#endif //SDLTEST_TASKQUEUE_H
