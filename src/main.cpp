#include "Agents.h"
#include "Canvas.h"
#include "Application.h"
#include "SDLWrapper.h"
#include <memory>


int main() {
#if SDLW_RUN_TIME > 0
    printf("Will run for %d seconds with %d agents\n", SDLW_RUN_TIME, AGENTS_COUNT);
#else
    printf("Will run indefinitely with %d agents\n", AGENTS_COUNT);
#endif

    SDLWrapper sdlWrapper;
    sdlWrapper.init();

    auto agents = std::make_shared<Agents>(sdlWrapper.bufferWidth, sdlWrapper.bufferHeight);
    auto canvas = std::make_shared<Canvas>(sdlWrapper.bufferWidth, sdlWrapper.bufferHeight);
    agents->init(AGENTS_COUNT);
    auto application = std::make_shared<Application>();
    application->init();
    application->setAgents(agents);
    application->setCanvas(canvas);

    sdlWrapper.setApplication(application);
    sdlWrapper.loop();

    application->quit();
    sdlWrapper.cleanup();
    return 0;
}
