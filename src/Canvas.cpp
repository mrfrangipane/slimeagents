#include "Canvas.h"

Canvas::Canvas(uint width_, uint height_) {
    width = width_;
    height = height_;
    pixelCount = width * height;
    printf("Buffer resolution is %dx%d\n", width, height);

    // Black pixel indexes
    blackPixelIndexes.clear();
    for (int x = 0; x < width; x++) {
        blackPixelIndexes.push_back(x);
        blackPixelIndexes.push_back((height -1) * width + x);
    }
    for (int y = 0; y < height; y++) {
        blackPixelIndexes.push_back(y * width);
        blackPixelIndexes.push_back(y * width + width - 1);
    }

    // Fill with black
    pixels.resize(pixelCount);
    pixelsBlur.resize(pixelCount);
    reset();
}

void Canvas::reset() {
    for (int p = 0; p < pixelCount; p++) {
        pixels[p] = 0xFF000000;
    }
    for (int p = 0; p < pixelCount; p++) {
        pixelsBlur[p] = 0xFF000000;
    }
}

void Canvas::paintAgents(const std::vector<Agent>& agents) {
    for (auto agent : agents) {
        auto x = static_cast<uint32_t>(agent.position.x);
        auto y = static_cast<uint32_t>(agent.position.y);

#if AGENTS_DRAW_AS_CROSS == 1
        if (x > 0) {
            pixels[x - 1 + y * width] |= agent.mask;
        }
        if (x < width - 1) {
            pixels[x + 1 + y * width] |= agent.mask;
        }
        if (y > 0) {
            pixels[x + (y - 1) * width] |= agent.mask;
        }
        if (y < height - 1) {
            pixels[x + (y + 1) * width] |= agent.mask;
        }
#endif

        pixels[x + y * width] |= agent.colorMaskRGB;
    }
}

void Canvas::blur(uint startRow, uint endRow) {
    uint32_t r;
    uint32_t g;
    uint32_t b;

    for (int p = startRow * width; p < endRow * width - 1; p++) {
        int x = p % static_cast<int>(width);
        if (x == 0) { continue; }

        r = 0; g = 0; b = 0;
        for (int ox = -1; ox < 2; ox++) {
            for (int oy = -1; oy < 2; oy++) {
                int offset = p + ox + oy * width;
                r += (pixels[offset] & 0x00FF0000) >> 16;
                g += (pixels[offset] & 0x0000FF00) >> 8;
                b += pixels[offset] & 0x000000FF;
            }
        }
        r = (r * 205) >> 11;
        g = (g * 205) >> 11;
        b = (b * 205) >> 11;

        pixelsBlur[p] = r << 16 | g << 8 | b;
    }
}

void Canvas::copyToTarget(uint startRow, uint endRow) {
    for (int p = startRow * width; p < endRow * width; p++) {
#if DEBUG_DO_BLUR == 1
        targetPixels[p] = pixelsBlur[p];
        pixels[p] = pixelsBlur[p]; // write to "backbuffer"
#else
        targetPixels[p] = pixels[p];
#endif
    }
}

void Canvas::paintBlackBorder() {
    for (auto i : blackPixelIndexes) {
#if DEBUG_DO_BLUR == 1
        pixelsBlur[i] = 0xFF000000;
#else
        pixels[i] = 0xFF000000;
#endif
    }
}

uint32_t Canvas::sample(Agent::Pos position) {
#if DEBUG_DO_BLUR == 1
    return pixelsBlur[static_cast<uint>(position.x) + static_cast<uint>(position.y) * width];
#else
    return pixels[static_cast<uint>(position.x) + static_cast<uint>(position.y) * width];
#endif
}
