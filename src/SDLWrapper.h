#ifndef SDLTEST_SDLWRAPPER_H
#define SDLTEST_SDLWRAPPER_H

#include <memory>
#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include "constants.h"
#include "Application.h"


class SDLWrapper {
private:
    TTF_Font* font;
    SDL_Window* pWindow = nullptr;
    SDL_Renderer* pRenderer = nullptr;
    SDL_Texture* pRenderBuffer = nullptr;
    SDL_Joystick* pGameController = nullptr;
    std::shared_ptr<Application> pApp = nullptr;
    int screenWidth = 0;
    int screenHeight = 0;
    bool drawHUD = false;
    bool running = false;
    SDL_Event event;
    Uint32 frameTicksDuration = 1;
    Uint32 loopTicksStart = 1;

    void initSDL();
    void initTextures();
    void processEvents();
    void renderApp();
    void renderHUD();
    void renderPresent();

public:
    int bufferWidth = 0;
    int bufferHeight = 0;
    Uint32 frameCount = 0;
    void init();
    void setApplication(std::shared_ptr<Application> application);
    void loop();
    void cleanup();
};


#endif //SDLTEST_SDLWRAPPER_H
