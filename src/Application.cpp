#include "Application.h"

void Application::init() {
#if DEBUG_THREADED == 1
    threadCount = std::thread::hardware_concurrency();
    printf("Thread count is %d\n", threadCount);

    for (int i = 0; i < threadCount; i++) {
        threads.emplace_back(&Application::threadLoop, this);
    }
    for (std::thread &thread : threads) {
        thread.detach();
    }
#else
    printf("Thread count is 0 (no threading)\n");
#endif
}

void Application::threadLoop() {
#if DEBUG_THREADED == 1
    while (taskQueue.running()) {
        BlurTask blurTask {};
        CopyToTargetTask copyToTargetTask {};
        UpdateAgentsTask updateAgentsTask {};

        if (taskQueue.next(blurTask)) {
            canvas->blur(blurTask.startRow, blurTask.endRow);
            taskQueue.taskDone();
        }

        else if (taskQueue.next(copyToTargetTask)) {
            canvas->copyToTarget(copyToTargetTask.startRow, copyToTargetTask.endRow);
            taskQueue.taskDone();
        }

        else if (taskQueue.next(updateAgentsTask)) {
            agents->update(updateAgentsTask.startAgent, updateAgentsTask.endAgent, canvas);
            taskQueue.taskDone();
        }

        else {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }
#endif
}

void Application::onKickEvent(uint32_t *pixels) {
#if DEBUG_THREADED == 1
    uint partialAgentCount = agents->agents.size() / threadCount;
    for (uint t = 0; t < threadCount; t++) {
        taskQueue.push(UpdateAgentsTask {t * partialAgentCount, (t + 1) * partialAgentCount});
    }
    while (taskQueue.busy()) { }
#else
    agents->update(0, agents->agents.size(), canvas);
#endif
    canvas->setTargetPixels(pixels);
    canvas->paintAgents(agents->agents);

#if DEBUG_THREADED == 1
    uint partialRowCount = canvas->height / threadCount;

#if DEBUG_DO_BLUR == 1
    for (uint t = 0; t < threadCount; t++) {
        taskQueue.push(BlurTask {t * partialRowCount, (t + 1) * partialRowCount});
    }
    while (taskQueue.busy()) { }
#endif

    canvas->paintBlackBorder();

    // Copy to SDL
    for (uint t = 0; t < threadCount; t++) {
        taskQueue.push(CopyToTargetTask {t * partialRowCount, (t + 1) * partialRowCount});
    }
    while (taskQueue.busy()) { }
#else
#if DO_BLUR == 1
    canvas->blur(0, canvas->height);
#endif // DO_BLUR
    canvas->paintBlackFrame();
    canvas->copyToTarget(0, canvas->height);
#endif // THREADED

    canvas->setTargetPixels(nullptr);
}

void Application::onGameControllerEvent(uint8_t axis, float value) {
#if DEBUG_PRINTF_GAMEPAD == 1
    printf("axis %d value %f\n", axis, value);
#endif

    if (axis == GAMEPAD_RIGHT_TRIG) {
        agents->setFov(value + 1.1);
    }
    else if (axis == GAMEPAD_LEFT_TRIG) {
        agents->setSpeed((value + 1.1) * 2.0);
    }
    else if (axis == GAMEPAD_BUTTON_X && value == 1.0) {
        agents->init(AGENTS_COUNT);
        canvas->reset();
    }
}
