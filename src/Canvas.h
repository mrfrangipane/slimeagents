#ifndef SDLTEST_CANVAS_H
#define SDLTEST_CANVAS_H

#include <algorithm>
#include <thread>
#include <cstring>
#include <cstdint>
#include <vector>
//#include "fastRandom.h"
#include "TaskQueue.h"
#include "constants.h"
#include "typedefs.h"


class Canvas {
private:
    std::vector<uint32_t> blackPixelIndexes;
    std::vector<uint32_t> pixels;
    std::vector<uint32_t> pixelsBlur;
    uint32_t* targetPixels;
    //FastRandom fastRandom;

public:
    uint width;
    uint height;
    uint pixelCount;

    Canvas(uint width, uint height);
    void reset();
    void paintAgents(const std::vector<Agent>& agents);
    void setTargetPixels(uint32_t* targetPixels_) { targetPixels = targetPixels_; }
    void blur(uint startRow, uint endRow);
    void copyToTarget(uint startRow, uint endRow);
    void paintBlackBorder();
    uint32_t sample(Agent::Pos position);
};


#endif //SDLTEST_CANVAS_H
