#include "Agents.h"

void Agents::init(int32_t count) {
    agents.clear();
    agents.resize(count);

    seed++;
    randEngine.seed(seed);
    randDistance = std::uniform_real_distribution<float>(0, sqrt(height * height + width * width) * AGENTS_REPARTITION_RADIUS * 0.5);
    randAngle = std::uniform_real_distribution<float>(0, 2.0 * M_PI);
    randFOVRange = std::uniform_real_distribution<float>(-0.5 * AGENTS_FOV_RANDOM_RANGE, 0.5 * AGENTS_FOV_RANDOM_RANGE);

    for (int i = 0; i < count; i++) {
        Agent newAgent;

        float angle = randAngle(randEngine);
        float distance = randDistance(randEngine);
        newAgent.position.x = width * 0.5 + cos(angle) * distance;
        newAgent.position.y = height * 0.5 + sin(angle) * distance;
        newAgent.direction =  angle;

        if (i < count * .33) {
            newAgent.colorMaskRGB = AGENTS_R_MASK;
        } else if (i < count * .66) {
            newAgent.colorMaskRGB = AGENTS_G_MASK;
        } else {
            newAgent.colorMaskRGB = AGENTS_B_MASK;
        }

        agents[i] = newAgent;
    }
}

void Agents::update(uint32_t start, uint32_t end, const std::shared_ptr<Canvas> canvas) {
    for (uint32_t i = start; i < end; i++) {

        // Probe
        Probe probe = newProbe(agents[i]);
        uint32_t frontValue = canvas->sample(probe.frontPos) & agents[i].colorMaskRGB;
        uint32_t leftValue = canvas->sample(probe.leftPos) & agents[i].colorMaskRGB;
        uint32_t rightValue = canvas->sample(probe.rightPos) & agents[i].colorMaskRGB;

        // Move
        if (leftValue > frontValue && leftValue > rightValue) {
            agents[i].position = probe.leftPos;
            agents[i].direction = probe.leftAngle;
        }
        else if (rightValue > frontValue && rightValue > leftValue) {
            agents[i].position = probe.rightPos;
            agents[i].direction = probe.rightAngle;
        }
        else if (frontValue > leftValue && frontValue > rightValue ) {
            agents[i].position = probe.frontPos;
            agents[i].direction = agents[i].direction;
        }
        else {
            agents[i].position = probe.frontPos;
            agents[i].direction = agents[i].direction + randFOVRange(randEngine);
        }

        // Bounce
        if (agents[i].position.x == 0.0) {
            agents[i].direction = randFOVRange(randEngine);
        }
        else if (agents[i].position.x == width) {
            agents[i].direction = M_PI + randFOVRange(randEngine);
        }

        if (agents[i].position.y == 0.0) {
            agents[i].direction = 0.5 * M_PI + randFOVRange(randEngine);
        }
        else if (agents[i].position.y == height) {
            agents[i].direction = 1.5 * M_PI + randFOVRange(randEngine);
        }

    }
}

Agent::Pos Agents::moveBy(Agent::Pos origin, float angle) { // TODO : find a better name and place
    Agent::Pos newPos;
    newPos.x = origin.x + std::cos(angle) * speed;
    newPos.y = origin.y + std::sin(angle) * speed;

    if (newPos.x < 0.0) { newPos.x = 0.0; }
    if (newPos.x > width) { newPos.x = width; }

    if (newPos.y < 0.0) { newPos.y = 0.0; }
    if (newPos.y > height) { newPos.y = height; }

    return newPos;
}

Probe Agents::newProbe(const Agent& agent) { // TODO : find a better place
    Probe probe;
    probe.frontPos = moveBy(agent.position, agent.direction);

    probe.leftAngle = agent.direction + fov * 0.5;
    probe.leftPos = moveBy(agent.position, probe.leftAngle);

    probe.rightAngle = agent.direction - fov * 0.5;
    probe.rightPos = moveBy(agent.position, probe.rightAngle);

    return probe;
}
