#include "SDLWrapper.h"
#include <utility>

void SDLWrapper::initSDL() {
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK);
    TTF_Init();

#if SDLW_BUFFER_SMOOTH_UPSCALE == 1
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
#endif

    SDL_DisplayMode displayMode;
    SDL_GetCurrentDisplayMode(0, &displayMode);
    screenWidth = displayMode.w;
    screenHeight = displayMode.h;
    bufferWidth = SDLW_BUFFER_WIDTH;
    bufferHeight = SDLW_BUFFER_HEIGHT;

#if SDLW_FULLSCREEN == 1
    pWindow = SDL_CreateWindow(
            SDLW_WINDOW_TITLE,
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            screenWidth, screenHeight,
            SDL_WINDOW_SHOWN
    );
    SDL_SetWindowFullscreen(pWindow, SDL_WINDOW_FULLSCREEN);
#else
    pWindow = SDL_CreateWindow(
            SDLW_WINDOW_TITLE,
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            bufferWidth, bufferHeight,
            SDL_WINDOW_SHOWN
    );
#endif
    pRenderer = SDL_CreateRenderer(
            pWindow,
            -1,
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE | SDL_RENDERER_PRESENTVSYNC
    );

    font = TTF_OpenFont("OpenSans-Regular.ttf", 24);
    if (!font) {
        SDL_LogError(
                SDL_LOG_CATEGORY_APPLICATION,
                "Failed to load font: %s",
                TTF_GetError()
        );
    }

    if (SDL_NumJoysticks() < 1) {
        SDL_LogWarn(
                SDL_LOG_CATEGORY_APPLICATION,
                "No joysticks connected"
        );
    } else {
        pGameController = SDL_JoystickOpen(0);
        if (pGameController == nullptr) {
            SDL_LogError(
                    SDL_LOG_CATEGORY_APPLICATION,
                    "Unable to open game controller: %s",
                    SDL_GetError()
            );
        }
    }
}

void SDLWrapper::initTextures() {
    pRenderBuffer = SDL_CreateTexture(
        pRenderer,
        SDL_PIXELFORMAT_RGB888,
        SDL_TEXTUREACCESS_STREAMING,
        bufferWidth, bufferHeight
    );
}

void SDLWrapper::processEvents() {
    while (SDL_PollEvent(&event)) {
        if (event.type == SDL_QUIT) {
            running = false;
        }
        else if (event.type == SDL_KEYDOWN) {
            SDL_Keycode key = event.key.keysym.sym;

            if (key == SDLK_ESCAPE) {
                running = false;
            }
            else if (key == SDLK_KP_MINUS || key == SDLK_h) {
                drawHUD = !drawHUD;
            }
        }
        else if (event.type == SDL_JOYAXISMOTION) {
            float value = ((float)event.jaxis.value / (float)INT16_MAX);
            pApp->onGameControllerEvent(event.jaxis.axis, value);
        }
        else if (event.type == SDL_JOYBUTTONDOWN) {
            pApp->onGameControllerEvent(event.jbutton.button + SDLW_GAMEPAD_BUTTON_OFFSET, 1.0);
            if (event.jbutton.button + SDLW_GAMEPAD_BUTTON_OFFSET == GAMEPAD_RIGHT_TRIG_UPPER) {
                drawHUD = !drawHUD;
            }
        }
        else if (event.type == SDL_JOYBUTTONUP) {
            pApp->onGameControllerEvent(event.jbutton.button + SDLW_GAMEPAD_BUTTON_OFFSET, 0.0);
        }
    }
}

void SDLWrapper::renderApp() {
    Uint32* pixels = nullptr;
    int pitch = 0;
    SDL_LockTexture(pRenderBuffer, nullptr, reinterpret_cast<void**>(&pixels), &pitch);
    pApp->onKickEvent(pixels);
    SDL_UnlockTexture(pRenderBuffer);
}

void SDLWrapper::renderHUD() {
    int width;
    int height;
    char text[200];
    int framerate = static_cast<int>(1000.0 / (float)frameTicksDuration);

    sprintf(text, "%d fps", framerate);

    SDL_Surface* HUD;
    SDL_Color color = { 255, 255, 255 };
    HUD = TTF_RenderText_Solid(font, text, color);
    SDL_Texture * texture = SDL_CreateTextureFromSurface(pRenderer, HUD);
    SDL_QueryTexture(texture, nullptr, nullptr, &width, &height);
    SDL_Rect rect = {20, 20, width, height};
    SDL_RenderCopy(pRenderer, texture, nullptr, &rect);

    SDL_DestroyTexture(texture);
    SDL_FreeSurface(HUD);
}

void SDLWrapper::renderPresent() {
    SDL_RenderClear(pRenderer);
    SDL_RenderCopy(pRenderer, pRenderBuffer, nullptr, nullptr);
    if (drawHUD) {
        renderHUD();
    }

    SDL_RenderPresent(pRenderer);
}

///////////////////////////////////////////////////////////

void SDLWrapper::init() {
    initSDL();
    initTextures();
}

void SDLWrapper::setApplication(std::shared_ptr<Application> application) {
    pApp = std::move(application);
}

void SDLWrapper::loop() {
    Uint32 frameTicksStart;
    Uint32 frameTicksEnd;
    loopTicksStart = SDL_GetTicks();
    running = true;

    while (running) {
        frameTicksStart = SDL_GetTicks();

        processEvents();
        renderApp();
        renderPresent();

#if SDLW_FORCE_SLEEP_MS > 0
        SDL_Delay(SDLW_SLEEP_MS);
#else
        SDL_Delay(5); // Improve for fixed framerate ?
#endif
        frameTicksEnd = SDL_GetTicks();
        frameTicksDuration = frameTicksEnd - frameTicksStart;
        frameCount++;

#if SDLW_RUN_TIME > 0
        if (static_cast<float>(frameTicksEnd) / 1000.0 > SDLW_RUN_TIME) {
            running = false;
        }
#endif
    }

    SDL_Log("Average framerate: %f", (float)frameCount / ((float)frameTicksEnd - (float)loopTicksStart) * 1000.0);
}

void SDLWrapper::cleanup() {
    SDL_DestroyTexture(pRenderBuffer);
    pRenderBuffer = nullptr;

    SDL_DestroyRenderer(pRenderer);
    pRenderer = nullptr;

    SDL_DestroyWindow(pWindow);
    pWindow = nullptr;

    SDL_JoystickClose(pGameController);
    pGameController = nullptr;

    TTF_CloseFont(font);
    font = nullptr;

    TTF_Quit();
    SDL_Quit();
}
