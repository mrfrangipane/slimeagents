#ifndef SDLTEST_AGENTS_H
#define SDLTEST_AGENTS_H

#include <chrono>
#include <random>
#include <vector>
#include <cstdint>
#include <cmath>
#include "constants.h"
#include "typedefs.h"
#include "fastRandom.h"
#include "Canvas.h"


class Agents {
    float width;
    float height;
    float fov = AGENTS_INITIAL_FOV;
    float speed = AGENTS_INITIAL_SPEED;
    unsigned seed = 0;
    std::default_random_engine randEngine;
    std::uniform_real_distribution<float> randAngle;
    std::uniform_real_distribution<float> randFOVRange;
    std::uniform_real_distribution<float> randDistance;
    Agent::Pos moveBy(Agent::Pos origin, float angle);
    Probe newProbe(const Agent& agent);

public:
    std::vector<Agent> agents;
    Agents(uint16_t width_, uint16_t height_) { width = static_cast<float>(width_); height = static_cast<float>(height_); }
    void init(int32_t count);
    Agent agent(uint32_t index) { return agents[index]; }
    void update(uint32_t start, uint32_t end, std::shared_ptr<Canvas> canvas);
    void setFov(float value) { fov = value; }
    void setSpeed(float value) { speed = value; }
};

#endif //SDLTEST_AGENTS_H
