#include "TaskQueue.h"

void TaskQueue::push(BlurTask blurTask) {
    std::lock_guard<std::mutex> lockGuard(mutex);
    queueBlur.push(blurTask);
    runningTaskCount++;
}

bool TaskQueue::next(BlurTask& blurTask) {
    std::lock_guard<std::mutex> lockGuard(mutex);
    if (queueBlur.empty()) {
        return false;
    }
    blurTask = queueBlur.front();
    queueBlur.pop();
    return true;
}

void TaskQueue::push(CopyToTargetTask copyToTargetTask) {
    std::lock_guard<std::mutex> lockGuard(mutex);
    queueCopyToTarget.push(copyToTargetTask);
    runningTaskCount++;
}

bool TaskQueue::next(CopyToTargetTask& copyToTargetTask) {
    std::lock_guard<std::mutex> lockGuard(mutex);
    if (queueCopyToTarget.empty()) {
        return false;
    }
    copyToTargetTask = queueCopyToTarget.front();
    queueCopyToTarget.pop();
    return true;
}

void TaskQueue::push(UpdateAgentsTask updateAgentsTask) {
    std::lock_guard<std::mutex> lockGuard(mutex);
    queueUpdateAgents.push(updateAgentsTask);
    runningTaskCount++;
}

bool TaskQueue::next(UpdateAgentsTask& updateAgentsTask) {
    std::lock_guard<std::mutex> lockGuard(mutex);
    if (queueUpdateAgents.empty()) {
        return false;
    }
    updateAgentsTask = queueUpdateAgents.front();
    queueUpdateAgents.pop();
    return true;
}
