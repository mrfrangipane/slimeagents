#ifndef SDLTEST_TYPEDEFS_H
#define SDLTEST_TYPEDEFS_H


struct Agent {
    struct Pos {
        float x = 0.0;
        float y = 0.0;
    };
    Pos position;
    float direction;
    uint32_t colorMaskRGB = 0x00FFFFFF;
};

struct Probe {
    Agent::Pos frontPos;
    Agent::Pos leftPos;
    Agent::Pos rightPos;
    float leftAngle = 0;
    float rightAngle = 0;
};


#endif //SDLTEST_TYPEDEFS_H
