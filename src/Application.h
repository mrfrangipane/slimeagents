#ifndef SDLTEST_APPLICATION_H
#define SDLTEST_APPLICATION_H

#include <cstdint>
#include <memory>
#include <utility>
#include <vector>
#include "Canvas.h"
#include "Agents.h"
#include "TaskQueue.h"

class Application {
    uint threadCount;
    TaskQueue taskQueue;
    std::vector<std::thread> threads;
    void threadLoop();

    std::shared_ptr<Agents> agents;
    std::shared_ptr<Canvas> canvas;

public:
    void init();
    void setAgents(std::shared_ptr<Agents> agents_) { agents = std::move(agents_); }
    void setCanvas(std::shared_ptr<Canvas> canvas_) { canvas = std::move(canvas_); }
    void onKickEvent(uint32_t* pixels);
    void onGameControllerEvent(uint8_t axis, float value);
    void quit() { taskQueue.stop(); }
};


#endif //SDLTEST_APPLICATION_H
